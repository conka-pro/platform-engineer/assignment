# API App Deployment: A Comprehensive Solution

I'm excited to share with you my Python API app deployment for the assignment. I took the liberty to go above and beyond the original assignment, which is one of the reasons why it took a bit longer. My hope is that you'll see this not as overkill, but as a showcase of different technologies.

I'm a big fan of Infrastructure as Code (IaC) and automation, and I can't wait to give you a demo of my solution and engage in a technical discussion on the topic.

## What I Configured Manually

- DigitalOcean Kubernetes Cluster
- Nginx Ingress Controller helm chart
- Cert-Manager helm chart
- Kubernetes Metrics Server
- Base SSH key for Ansible added to DigitalOcean
- Personal SSH key added to DigitalOcean
- `env` variables added to GitLab CI/CD project settings (e.g., `TF_VAR_*`, `KUBECONFIG`, `DIGITALOCEAN_TOKEN`, SSH keys)
- Authenticated the K8s cluster with GitLab Container Registry

## Amendments in the App

### Python API

- Fixed an error in `create_db.py` and `drop_db.py`
- Added `entrypoint.sh` that waits for the DB to be ready, runs `create_db.py`, and starts the app
- Created `preStop.sh` as per assignment, configured in the K8s deployment as a preStop hook with a grace period of 45s

### MariaDB

- Added entrypoint script to create a read-only user for the DB (per assignment)

## K8s

- Deployment automated with Terraform, backend set as GitLab
- Automated in GitLab CI/CD pipeline
- Dynamic subdomains and SSL certificates managed with Let's Encrypt
- Certs managed by cert-manager in the cluster
- Nginx configured for redirection and basic auth
- Python API app deployed in 2 replicas, load balanced by K8s Service
- Redis & MariaDB deployed with persistence using K8s PVC
- Readiness & liveness probes set on all apps

## Bastion

- Special Digital Ocean VM defined in `bastion.tf`
- Used as entrypoint for managing K8s cluster
- Users setup according to the assignment, tools & users setup done with Ansible
- K9s tool preconfigured for cluster management
- Ansible playbook in `ansible/bastion.yaml`, run from GitLab CI/CD pipeline
- Bastion also available on `bastion.conka.tech` for production environment and on subdomains based on the feature branch name

## CI/CD Pipeline

Stages:

1. Validate: Check Terraform files for formatting and syntax errors
2. Build: Create Docker images and push to GitLab Container Registry
3. Security: Scan Docker images for vulnerabilities
4. Deploy: Deploy to K8s cluster, provision bastion node
5. Deploy Bastion: Run Ansible playbook for bastion node
6. Destroy: Environments automatically destroyed (configurable)

## Passwords

Admin app credentials and MariaDB readonly user credentials are on the bastion node `bastion.conka.tech` in `~/passwords.txt` file in `phrase_admin` user's home directory.

## Potential Improvements

If I had more time I would implement some of these ideas:

- Digital Ocean K8s cluster, domain settings, and Helm charts as IaC
- Kubernetes RBAC, special account for GitLab CI/CD
- High-availability database and Redis with K8s operators
- Secret rotation and encryption, DB backups
- Ansible Vault and host key checking
- Alerts for image vulnerabilities
- Setup non-root user for the ansible
- Use Hashicorp Packer for the base bastion image and ansible for updates

