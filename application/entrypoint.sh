#!/bin/sh 

# Wait for the database to start up
echo "Waiting for database to start up..."
while ! nc -z $DATABASE_HOST 3306; do
  sleep 1
done
echo "Database is up!"

echo "Running migration script..."
/usr/local/bin/python create_db.py

echo "Starting the application..."
/usr/local/bin/python app.py