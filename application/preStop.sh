#!/bin/sh

function check_preparing {
    if [ "$(curl -s 127.0.0.1:5000/prepare-for-deploy)" = "preparing" ]; then
        return 0
    else
        return 1
    fi
}

function check_ready {
    if [ "$(curl -s 127.0.0.1:5000/ready-for-deploy)" = "Ready" ]; then
        return 0
    else
        return 1
    fi
}

while ! check_preparing; do
    sleep 1
done

while ! check_ready; do
    sleep 1
done

exit 0
