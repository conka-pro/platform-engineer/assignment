variable "MYSQL_ROOT_PASSWORD" {}

variable "MYSQL_PASSWORD" {}

variable "MYSQL_USER" {}

variable "MYSQL_DATABASE" {}

variable "MYSQL_READONLY_USER" {}

variable "MYSQL_READONLY_PASSWORD" {}

variable "APP_ADMIN_USERNAME" {}

variable "APP_ADMIN_PASSWORD" {}

variable "namespace" {}

variable "api_image_tag" {}

variable "mariadb_image_tag" {}

variable "gl_ansible_ssh_public_key_path" {}