resource "kubernetes_manifest" "issuer_letsencrypt_nginx" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Issuer"
    "metadata" = {
      "name"      = "letsencrypt-nginx"
      "namespace" = kubernetes_namespace_v1.namespace.id
    }
    "spec" = {
      "acme" = {
        "email" = "filip@conka.pro"
        "privateKeySecretRef" = {
          "name" = "letsencrypt-nginx-private-key"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "nginx"
              }
            }
          },
        ]
      }
    }
  }
}