resource "kubernetes_deployment_v1" "api_deployment" {
  metadata {
    name      = "api"
    namespace = kubernetes_namespace_v1.namespace.id
    labels = {
      app = "api"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "api"
      }
    }

    template {
      metadata {
        labels = {
          app = "api"
        }
      }

      spec {
        image_pull_secrets {
          name = "gitlab-registry"
        }

        termination_grace_period_seconds = 45

        container {
          name  = "api"
          image = var.api_image_tag

          resources {
            limits = {
              cpu    = "500m"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "256Mi"
            }
          }

          readiness_probe {
            http_get {
              path = "/status"
              port = 5000
            }
            initial_delay_seconds = 5
            period_seconds        = 10
            timeout_seconds       = 5
            success_threshold     = 1
            failure_threshold     = 5
          }

          liveness_probe {
            http_get {
              path = "/status"
              port = 5000
            }
            initial_delay_seconds = 30
            period_seconds        = 30
            timeout_seconds       = 5
            success_threshold     = 1
            failure_threshold     = 3
          }

          lifecycle {
            pre_stop {
              exec {
                command = ["/bin/sh", "-c", "/home/app/preStop.sh"]
              }
            }
          }

          env {
            name  = "APP_HOST"
            value = "0.0.0.0"
          }

          env {
            name  = "APP_PORT"
            value = "5000"
          }

          env {
            name  = "REDIS_HOST"
            value = "redis"
          }

          env {
            name  = "REDIS_PORT"
            value = "6379"
          }

          env {
            name = "DATABASE_URI"
            value_from {
              secret_key_ref {
                name = resource.kubernetes_secret_v1.mariadb_secret.metadata[0].name
                key  = "uri"
              }
            }
          }

          env {
            name  = "DATABASE_HOST"
            value = "mariadb"
          }

          env {
            name = "MYSQL_DATABASE"
            value_from {
              secret_key_ref {
                name = resource.kubernetes_secret_v1.mariadb_secret.metadata[0].name
                key  = "database"
              }
            }
          }

          env {
            name = "MYSQL_USER"
            value_from {
              secret_key_ref {
                name = resource.kubernetes_secret_v1.mariadb_secret.metadata[0].name
                key  = "user"
              }
            }
          }

          env {
            name = "MYSQL_PASSWORD"
            value_from {
              secret_key_ref {
                name = resource.kubernetes_secret_v1.mariadb_secret.metadata[0].name
                key  = "password"
              }
            }
          }

          port {
            container_port = 5000
          }
        }
      }
    }
  }
}


resource "kubernetes_service_v1" "api" {
  metadata {
    name      = "api"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    selector = {
      app = "api"
    }

    port {
      name        = "api"
      port        = 5000
      target_port = 5000
    }
  }
}

resource "kubernetes_ingress_v1" "ingress_api" {
  metadata {
    name      = "ingress-api"
    namespace = kubernetes_namespace_v1.namespace.id

    annotations = {
      "cert-manager.io/issuer"                         = "letsencrypt-nginx"
      "nginx.ingress.kubernetes.io/force-ssl-redirect" = "true"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      hosts = [
        format("%s.conka.tech", var.namespace)
      ]

      secret_name = "letsencrypt-nginx"
    }

    rule {
      host = format("%s.conka.tech", var.namespace)

      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "api"
              port {
                number = 5000
              }
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_ingress_v1" "ingress_api_restricted" {
  metadata {
    name      = "ingress-api-restricted"
    namespace = kubernetes_namespace_v1.namespace.id

    annotations = {
      "cert-manager.io/issuer"                         = "letsencrypt-nginx"
      "nginx.ingress.kubernetes.io/force-ssl-redirect" = "true"
      "nginx.ingress.kubernetes.io/permanent-redirect" = "/"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      hosts = [
        format("%s.conka.tech", var.namespace)
      ]

      secret_name = "letsencrypt-nginx"
    }

    rule {
      host = format("%s.conka.tech", var.namespace)

      http {
        path {
          path      = "/prepare-for-deploy"
          path_type = "Prefix"
          backend {
            service {
              name = "api"
              port {
                number = 5000
              }
            }
          }
        }

        path {
          path      = "/ready-for-deploy"
          path_type = "Prefix"
          backend {
            service {
              name = "api"
              port {
                number = 5000
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_secret_v1" "api_basic_auth" {
  type = "Opaque"
  metadata {
    name      = "api-basic-auth"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  data = {
    "auth" : format("%s:%s", var.APP_ADMIN_USERNAME, bcrypt(var.APP_ADMIN_PASSWORD))
  }
}


resource "kubernetes_ingress_v1" "ingress_api_admin" {
  metadata {
    name      = "ingress-api-admin"
    namespace = kubernetes_namespace_v1.namespace.id

    annotations = {
      "cert-manager.io/issuer"                         = "letsencrypt-nginx"
      "nginx.ingress.kubernetes.io/force-ssl-redirect" = "true"
      "nginx.ingress.kubernetes.io/auth-type"          = "basic"
      "nginx.ingress.kubernetes.io/auth-secret"        = kubernetes_secret_v1.api_basic_auth.metadata[0].name
      "nginx.ingress.kubernetes.io/auth-realm"         = "Authentication required"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      hosts = [
        format("%s.conka.tech", var.namespace)
      ]

      secret_name = "letsencrypt-nginx"
    }

    rule {
      host = format("%s.conka.tech", var.namespace)

      http {
        path {
          path      = "/admin"
          path_type = "Prefix"
          backend {
            service {
              name = "api"
              port {
                number = 5000
              }
            }
          }
        }
      }
    }
  }
}

output "api_url" {
  value = format("https://%s.conka.tech", var.namespace)
}