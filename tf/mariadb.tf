resource "kubernetes_secret_v1" "mariadb_root_secret" {
  metadata {
    name      = "mariadb-root-secret"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  data = {
    root-password = var.MYSQL_ROOT_PASSWORD
  }
}

resource "kubernetes_secret_v1" "mariadb_secret" {
  metadata {
    name      = "mariadb-secret"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  data = {
    user              = var.MYSQL_USER
    database          = var.MYSQL_DATABASE
    password          = var.MYSQL_PASSWORD
    readonly-user     = var.MYSQL_READONLY_USER
    readonly-password = var.MYSQL_READONLY_PASSWORD

    uri = format("mysql+pymysql://%s:%s@mariadb:3306/%s", var.MYSQL_USER, var.MYSQL_PASSWORD, var.MYSQL_DATABASE)
  }
}

resource "kubernetes_persistent_volume_claim_v1" "mariadb_pvc" {
  metadata {
    name      = "mariadb-pvc"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    access_modes = ["ReadWriteOnce"]

    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubernetes_deployment_v1" "mariadb" {
  metadata {
    name      = "mariadb"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    replicas = 1

    strategy {
      type = "Recreate"
    }

    selector {
      match_labels = {
        app = "mariadb"
      }
    }

    template {
      metadata {
        labels = {
          app = "mariadb"
        }
      }

      spec {
        image_pull_secrets {
          name = "gitlab-registry"
        }

        container {
          name  = "mariadb"
          image = var.mariadb_image_tag

          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = "mariadb-root-secret"
                key  = "root-password"
              }
            }
          }

          env {
            name = "MYSQL_DATABASE"
            value_from {
              secret_key_ref {
                name = "mariadb-secret"
                key  = "database"
              }
            }
          }

          env {
            name = "MYSQL_USER"
            value_from {
              secret_key_ref {
                name = "mariadb-secret"
                key  = "user"
              }
            }
          }

          env {
            name = "MYSQL_PASSWORD"
            value_from {
              secret_key_ref {
                name = "mariadb-secret"
                key  = "password"
              }
            }
          }

          env {
            name = "MYSQL_READONLY_USER"
            value_from {
              secret_key_ref {
                name = "mariadb-secret"
                key  = "readonly-user"
              }
            }
          }

          env {
            name = "MYSQL_READONLY_PASSWORD"
            value_from {
              secret_key_ref {
                name = "mariadb-secret"
                key  = "readonly-password"
              }
            }
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "512Mi"
            }
            limits = {
              cpu    = "500m"
              memory = "1Gi"
            }
          }

          liveness_probe {
            exec {
              command = [
                "/bin/bash",
                "-c",
                "mysql -h 127.0.0.1 -u\"$MYSQL_USER\" -p\"$MYSQL_PASSWORD\" -e \"SELECT 1\""
              ]
            }
            initial_delay_seconds = 30
            period_seconds        = 10
            timeout_seconds       = 5
          }

          readiness_probe {
            exec {
              command = [
                "/bin/bash",
                "-c",
                "mysql -h 127.0.0.1 -u\"$MYSQL_USER\" -p\"$MYSQL_PASSWORD\" -e \"SELECT 1\""
              ]
            }
            initial_delay_seconds = 10
            period_seconds        = 5
            timeout_seconds       = 1
          }

          port {
            container_port = 3306
          }

          volume_mount {
            name       = "mariadb-data"
            mount_path = "/var/lib/mysql"
          }
        }

        volume {
          name = "mariadb-data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.mariadb_pvc.metadata.0.name
          }
        }
      }
    }
  }
}


resource "kubernetes_service_v1" "mariadb" {
  metadata {
    name      = "mariadb"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    selector = {
      app = "mariadb"
    }

    port {
      name        = "mysql"
      port        = 3306
      target_port = 3306
    }
  }
}
