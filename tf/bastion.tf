data "digitalocean_vpc" "cluster_network" {
  name = "default-fra1"
}

data "digitalocean_ssh_key" "gitlab_ansible" {
  name = "gitlab-ansible"
}

data "digitalocean_ssh_key" "conka" {
  name = "conka"
}

resource "digitalocean_droplet" "bastion" {
  image  = "ubuntu-22-10-x64"
  name   = format("%s-bastion", var.namespace)
  region = "fra1"
  size   = "s-1vcpu-1gb-amd"

  vpc_uuid = data.digitalocean_vpc.cluster_network.id

  ssh_keys = [
    data.digitalocean_ssh_key.gitlab_ansible.id,
    data.digitalocean_ssh_key.conka.id
  ]
}

data "digitalocean_domain" "conka_tech" {
  name = "conka.tech"
}

resource "digitalocean_record" "bastion" {
  type   = "A"
  domain = data.digitalocean_domain.conka_tech.id
  name   = var.namespace != "api" ? format("%s.bastion", var.namespace) : "bastion"
  value  = resource.digitalocean_droplet.bastion.ipv4_address
}

output "bastion-ip" {
  value = resource.digitalocean_droplet.bastion.ipv4_address
}