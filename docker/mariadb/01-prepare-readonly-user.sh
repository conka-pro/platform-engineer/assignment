#!/bin/bash

if [ -z "$MYSQL_READONLY_USER" ] || [ -z "$MYSQL_READONLY_PASSWORD" ] || [ -z "$MYSQL_DATABASE" ]; then
  echo "Error: MYSQL_READONLY_USER, MYSQL_READONLY_PASSWORD, and MYSQL_DATABASE environment variables must be set."
  exit 1
fi

readonly_user="$MYSQL_READONLY_USER"
readonly_password="$MYSQL_READONLY_PASSWORD"
database="$MYSQL_DATABASE"
root_password="$MYSQL_ROOT_PASSWORD"

# Create the read-only user
mysql -uroot -p"${root_password}" -e "GRANT SELECT ON ${database}.* TO '${readonly_user}'@'%' IDENTIFIED BY '${readonly_password}';"